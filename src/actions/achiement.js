import {TEST} from '../constants/actionTypes'

const testIncrement = () => ({
    type: TEST,
    payload: 1
});

export {
    testIncrement
}