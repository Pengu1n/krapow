import { TEST } from '../constants/actionTypes'

const init = {
    atamagaitai: 0,
    disease2Number: 0
}

const checkUnlocked = (num, disease) => {
    if(num === 10) {
        alert('Wow : '+ disease);
    }
}

function testRedux(state= init, action) {
    switch(action.type){
        case TEST: {
            for(let disease in init){
                if (disease === 'atamagaitai'){
                    console.log(disease);
                    checkUnlocked(state.atamagaitai, disease);
                    break;
                }
                else if(disease === 'disease2Number'){
                    console.log(disease);
                    break;
                }
                else{
                    alert('Why')
                }
            }
            return {
                ...state,
                atamagaitai: state.atamagaitai + action.payload
            }
        }
        default: return state
    }
}

export default testRedux;