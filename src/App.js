import React from 'react';
import logo from './logo.svg';
import './App.css';

import {connect} from 'react-redux';

import {testIncrement} from './actions/achiement';

const mapStateToProps = state => ({
  // stories in first parameter
  number: state.achivementState.atamagaitai,
});

// Dispatch = action/ function as a props
const mapDispatchToProps = dispatch => ({
    // onArchive in second parameter
  increment: () => dispatch(testIncrement()),
});

function App({number, increment}) {


  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <button onClick={increment}>
          Click me
        </button>
        {number}

      </header>
    </div>
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

